# Poker Hands

This is a solution for the **Poker Hands** assignment in the CPP Embedded Development Bootcamp. The program should pick **5** cards out of **2** 52-card decks and display the possible poker hand of said cards.

## Table of Contents

 - [Requirements](#requirements)
 - [Usage](#usage)
 - [Maintainers](#maintainers)
 - [Contributing](#contributing)
 - [License](#license)

## Requirements

Requires `cmake` and `gcc`

## Usage

	$ cd build
	$ cmake ../
	$ make
	$ ./main

## Notes

I ran out of time so the program only checks if the user has pairs, or 3 or more of a kind.

## Maintainers

[Elmo Rohula @rootElmo](https://gitlab.com/rootElmo)

## Contributing

## License
