#include <iostream>
#include <array>
#include <stdlib.h>
#include <string>
#include <time.h>

/*
TODO:
Separate poker hand checking logic to another file.
*/


/*Two decks of cards as 2-dimensional array*/
std::array<std::array<bool, 52>, 2> card_decks;

/*TODO: change this to class
since we are doing c++ not c :-)*/
struct card {
	int value;
	int suit;	// 0 = Spades; 1 = Hearts; 2 = Clubs; 3 = Diamonds
	int deck;	// Possibly reduntant?
};

/*Cards that are 'dealt' to the 'player'*/
std::array<card, 5> table_cards;

std::array<std::string, 13> card_names = {"Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"};

std::array<std::string, 4> card_suits = {"Spades", "Hearts", "Clubs", "Diamonds"};

/*Get random card from either deck for specified table card*/
void get_random_card(int card) {
	bool got_card = false;

	/*Init random num generator with seed based on current time*/
	srand (time(NULL));
	do {
		int new_card_value = rand() % 52;
		int from_pack = rand() % 2;
		int new_suit=0;
		if (!card_decks[from_pack][new_card_value]) {
			card_decks[from_pack][new_card_value] = true;
			
			/*Card values in the range of 0-13 (13 different cards)*/
			if (new_card_value>12) new_suit = new_card_value / 13; 
			if (new_card_value>12) new_card_value = new_card_value % 13;
			
			table_cards[card].value = new_card_value;
			table_cards[card].suit = new_suit;
			table_cards[card].deck = from_pack;

			got_card = true;
		}
	} while (!got_card);
}


void print_cards() {
	for (int i=0; i<5; i++) {
		std::cout << "Card " << i << ": ";
		std::cout << card_names[table_cards[i].value] << " of "
			<< card_suits[table_cards[i].suit] << "\n";
	}
}

/*Get highest value card of the table*/
int high_card() {
	int highest_card = table_cards[0].value;
	for (int i=0; i<5; i++) {
		if (table_cards[i].value > highest_card) highest_card = table_cards[i].value;
	}
	return highest_card;
}

/*Check amount of pairs*/
int check_pairs() {
	/*Card we are comparing with*/
	int pairs_amnt = 0;
	int pair_one = -1;
	for (int i=0; i<5; i++) {
		/*Card we are comparing to*/
		for (int j=i+1; j<5;j++) {
			if (table_cards[i].value == table_cards[j].value) {
				if (pair_one == -1) {
					pair_one = table_cards[i].value;
					pairs_amnt = 1;
				} else if (pair_one != -1 && table_cards[j].value != pair_one )
					pairs_amnt = 2;
			/*A pair is found*/
				//std::cout << "You have got a pair! (of cards)\n";
				//print_cards();
				//return;
			}
		}
	}
	return pairs_amnt;
}

/*Checks if there are 3 or more cards of the same value*/
int check_sames() {
	int sames_amnt = 0;
	for (int i=0; i<5; i++) {
		for (int j=i+1; j<5; j++) {
			if (table_cards[i].value == table_cards[j].value) sames_amnt++;
		}
	}
	if (sames_amnt >= 3) {
		return sames_amnt;
	}
	return 0;
}

void get_poker_hand() {
	int pairs = check_pairs();
	int sames = check_sames();
	/*Parse card values*/	
	
	if (sames != 0) {
		std::cout << "You got " << sames << " of a kind!\n";
		print_cards();
		return;
	} else if (pairs != 0) {
		std::cout << "You got " << pairs << " pairs!\n";
		print_cards();
		return;
	}

	/*Prints highest card if no other hand found*/
	std::cout << "Highest card: " << high_card() << "\n";
	print_cards();
}

int main() {
	/*Fill table cards*/
	for (int i=0; i<5; i++) {
		get_random_card(i);
	}
	/*Check for poker hands*/
	get_poker_hand();
	return 0;
}
